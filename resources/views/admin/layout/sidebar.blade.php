<aside class="main-sidebar elevation-4 sidebar-light-lime">
    <a href="{{ URL::to('/') }}" class="brand-link">
        <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">APOIO HUMANITARIO</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item menu-open">
                    <a href="#" class="nav-link">
                        <p>
                            <i class="fa fa-lightbulb text-success"></i> {{ Auth::user()->username }} 
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                                <i class="fa fa-power-off"></i> Terminar Sessão 
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item {{ Request::is('submission/create') ? 'menu-open' : '' }} {{ Request::is('submission/listsubmissions') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Request::is('submission/create') ? 'active' : '' }} {{ Request::is('submission/listsubmissions') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Submissões
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ URL::to('submission/create') }}" class="nav-link {{ Request::is('submission/create') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Adicionar Submissão</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('submission/listsubmissions') }}" class="nav-link {{ Request::is('submission/listsubmissions') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Lista Submissões</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::is('associations/create') ? 'menu-open' : '' }} {{ Request::is('associations/listassociations') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Request::is('associations/create') ? 'active' : '' }} {{ Request::is('associations/listassociations') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Associações
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ URL::to('associations/create') }}" class="nav-link {{ Request::is('associations/create') ? 'active' : '' }} ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Adicionar</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href="{{ URL::to('associations/listassociations') }}" class="nav-link {{ Request::is('associations/listassociations') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Associações Registadas</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::is('company/create') ? 'menu-open' : '' }} {{ Request::is('company/listcompanies') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link  {{ Request::is('company/create') ? 'active' : '' }} {{ Request::is('company/listcompanies') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Instituições
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ URL::to('company/create') }}" class="nav-link {{ Request::is('company/create') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Adicionar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('company/listcompanies') }}" class="nav-link {{ Request::is('company/listcompanies') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Instituições Registadas</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::is('users/create') ? 'menu-open' : '' }} {{ Request::is('users/listusers') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Request::is('users/create') ? 'active' : '' }} {{ Request::is('users/listusers') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Usuarios
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ URL::to('users/create') }}" class="nav-link {{ Request::is('users/create') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Adicionar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('users/listusers') }}" class="nav-link {{ Request::is('users/listusers') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Usuarios Registados</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>
                                    Controlo de Acesso
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-dot-circle nav-icon"></i>
                                        <p>Level 3</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-dot-circle nav-icon"></i>
                                        <p>Level 3</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-dot-circle nav-icon"></i>
                                        <p>Level 3</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Definicões
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ URL::to('settings/application') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Aplicação</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ URL::to('settings/pages') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Páginas</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ URL::to('settings/general') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sistema</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>
