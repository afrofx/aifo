<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.layout.meta')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('admin.layout.nav-bar')
        @include('admin.layout.sidebar')
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>@yield('add-title')</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="/">INICIO</a></li>
                                @yield('breadcrumb')
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
    
            <section class="content">
                @yield('main-content')
            </section>
        </div>
        @include('admin.layout.footer')
    </div>

    @include('admin.layout.script')
</body>

</html>
