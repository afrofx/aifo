<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="{{asset('plugins/fontawesome/css/all.min.css')}}">
@yield('add-meta')
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<style>
    .btn, .card, .badge, .small-box {
        border-radius: 0 !important;
    }

    .card-teal:not(.card-outline)>.card-header {
        background-color: #8bc34a !important;
    }

    .bg-teal{
        background-color: #8bc34a !important;
    }

    .sidebar-light-lime .nav-sidebar>.nav-item>.nav-link.active {
        background-color: #8bc34a !important;
        color: #1f2d3d;
    }
    /* .card-teal{
        background-color: #8bc34a !important;
    } */
</style>
<title>@yield('add-title')</title>