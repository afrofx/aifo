@extends('admin.layout.main')
@section('add-title')
    Adiconar Submissão
@endsection

@section('add-meta')
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ URL::to('associations/listassociations') }}">Lista de Submissão</a></li>
    <li class="breadcrumb-item active">Adiconar Submissão</li>
@endsection

@section('main-content')
    <div class="container-fluid">
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Alerta!</h5> {{ Session::get('success') }}
            </div>
        @endif
        {!! Form::open(['route' => 'users.save']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-teal">
                    <div class="card-header">
                        <h3 class="card-title">Adicionar nova submissão</h3>
                    </div>

                    <form>
                        <div class="card-body">

                            <h4><b>Dados do Remetente<b></h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="id-number">Nº Bilhete de Identifição:</label>
                                        <input type="text" class="form-control" id="id-number" name="id-number"
                                            placeholder="Ex: 1111002002000C">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Nome Completo:</label>
                                        <input type="text" class="form-control" id="name" name="name"
                                            placeholder="Ex: Manuel Gaspar Mabote">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="birthday">Data de Nascimento:</label>
                                        <input type="date" class="form-control" id="birthday" name="birthday">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gender">Género:</label>
                                        <select class="form-control" name="gender" id="gender">
                                            <option selected disabled hidden>Escolha o Género</option>
                                            <option value="Femenino">Femenino</option>
                                            <option value="Masculino">Masculino</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="marital">Estado Civil:</label>
                                        <select class="form-control" name="marital" id="marital">
                                            <option selected disabled hidden>Escolha o Estado Civil</option>
                                            <option value="Solteiro">Solteiro</option>
                                            <option value="Casado">Casado</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="province">Província:</label>
                                        <select class="form-control" name="province" id="province">
                                            <option selected disabled hidden>Selecionar a Provincia</option>
                                            <option value="Solteiro">Maputo</option>
                                            <option value="Casado">Gaza</option>
                                            {{-- @foreach ($provinces as $province)
                                                    <option value="{{$province->id}}">{{$province->id}}</option>
                                                @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address">Residência:</label>
                                        <input type="address" class="form-control" id="address" name="address">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="contact">Contacto:</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="contact">Contacto Alternativo:</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>
                                </div>
                            </div>

                            <h4><b>Informações Sobre a Submissão<b></h4>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Descrição:</label>
                                        <input type="text" class="form-control" id="description" name="description"
                                            placeholder="Ex: Angariação de Fundos para as vitimas do IDAI">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="in-association">Pertence Alguma Associação?</label>
                                        <select class="form-control" name="in-association" id="in-association">
                                            <option value="Nao">Não Pertence</option>
                                            <option value="Sim">Pertence</option>
                                            {{-- @foreach ($provinces as $province)
                                                    <option value="{{$province->id}}">{{$province->id}}</option>
                                                @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="association">Nome da Associção:</label>
                                        <input type="text" class="form-control" id="association" name="association"
                                            placeholder="Ex: MUSAUDE" required aria-autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="province">Província:</label>
                                        <select class="form-control" name="province" id="province">
                                            <option selected disabled hidden>Selecionar a Provincia</option>
                                            <option value="Solteiro">Maputo</option>
                                            <option value="Casado">Gaza</option>
                                            {{-- @foreach ($provinces as $province)
                                                    <option value="{{$province->id}}">{{$province->id}}</option>
                                                @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="location">Localização:</label>
                                        <input type="location" class="form-control" id="location" name="location">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="files">Carregar Arquivos de Media</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="files" name="files">
                                                <label class="custom-file-label" for="files">Carregar Arquivos</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Carregar</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="details">Descrição Detalhada da Submissão:</label>
                                        <textarea id="summernote" name="details" cols="4">
                                            Place <em>some</em> <u>text</u> <strong>here</strong>
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="status" name="status" checked>
                                <label class="form-check-label" for="status">Activar Usuário</label>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-lg">Guardar e Adicionar Nova</button>
                            <button type="submit" class="btn btn-success btn-lg">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('add-script')
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(function () {
      $('#summernote').summernote();
    })
</script>
@endsection
