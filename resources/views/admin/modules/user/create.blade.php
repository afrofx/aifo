@extends('admin.layout.main')
@section('add-title')
    Adicionar Usuário
@endsection

@section('add-meta')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ URL::to('users/listusers') }}">Lista de Usuários</a></li>
    <li class="breadcrumb-item active">Adicionar Usuário</li>
@endsection

@section('main-content')
<div class="container-fluid">
	@if(Session::has('success'))
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h5><i class="icon fas fa-check"></i> Alerta!</h5> {{ Session::get('success') }}
		</div>
	@endif
	{!! Form::open(['route' => 'users.save']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-teal">
				<div class="card-header">
					<h3 class="card-title">Adicionar novos usuários no sistema</h3>
				</div>
				
				<form>
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Primeiro Nome:</label>
									<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Ex: Manuel">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Apelido:</label>
									<input type="text" class="form-control" id="surname" name="surname" placeholder="Ex: Mabote">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="username">Nome do Usuário:</label>
									<input type="text" class="form-control" id="username" name="username" placeholder="Ex: JMABOTE" required aria-autocomplete="new-password">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="email" class="form-control" id="email" name="email" placeholder="examplo@exemplo.co.mz">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="password">Senha:</label>
									<input type="password" class="form-control" id="password" name="password" placeholder="Escreva a senha" required autocomplete="new-password">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="repeat-password">Repetir Senha:</label>
									<input type="password" class="form-control" id="repeat-password"" name="repeat-password" placeholder="Escreva novamente a senha" required aria-autocomplete="new-password">
									<span id="message"></span>
								</div>
							</div>
						</div>

						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="status" name="status" checked>
							<label class="form-check-label" for="status">Activar Usuário</label>
						</div>
					</div>

					<div class="card-footer">
						<button type="submit" class="btn btn-primary btn-lg">Guardar e Adicionar Novo</button>
						<button type="submit" class="btn btn-success btn-lg">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection

@section('add-script')
@endsection
