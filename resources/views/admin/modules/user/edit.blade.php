@extends('admin.layout.main')
@section('add-title')
    Editar Usuário
@endsection

@section('add-meta')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{URL::to('users/listusers')}}">Lista de Usuários</a></li>
    <li class="breadcrumb-item active">Editar Usuário</li>
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
        </div>
    </div>
@endsection

@section('add-script')
@endsection
