@extends('admin.layout.main')
@section('add-title')
    Editar Associação
@endsection

@section('add-meta')
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL::to('associations/listassociations') }}">Lista de Associações</a></li>
    <li class="breadcrumb-item active">Editar Associação</li>
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
        </div>
    </div>
@endsection

@section('add-script')
@endsection
