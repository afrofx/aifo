@extends('admin.layout.main')
@section('add-title')
    Adiconar Associação
@endsection

@section('add-meta')
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ URL::to('associations/listassociations') }}">Lista de Associações</a></li>
    <li class="breadcrumb-item active">Adiconar Associação</li>
@endsection

@section('main-content')
<div class="container-fluid">
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alerta!</h5> {{ Session::get('success') }}
        </div>
    @endif
    {!! Form::open(['route' => 'users.save']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-teal">
                <div class="card-header">
                    <h3 class="card-title">Adicionar nova Associação</h3>
                </div>

                <form>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nuit">NUIT:</label>
                                    <input type="text" class="form-control" id="nuit" name="nuit" placeholder="Ex: 1111002">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Nome:</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        placeholder="Ex: Associação de Menores">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="initials">Sigla</label>
                                    <input type="text" class="form-control" id="initials" name="initials" placeholder="AASOC">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="activity">Actividade Principal</label>
                                    <input type="text" class="form-control" id="activity" name="activity">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="province">É uma assciação nacional:</label>
                                    <select class="form-control" name="province" id="province">
                                        <option value="Sim">Sim</option>
                                        <option value="Nao">Não</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="province">Província:</label>
                                    <select class="form-control" name="province" id="province">
                                        <option selected disabled hidden>Selecionar a Provincia</option>
                                        <option value="1">Maputo</option>
                                        <option value="2">Gaza</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="address">Endereço:</label>
                                    <input type="address" class="form-control" id="address" name="address">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="contact">Contacto:</label>
                                    <input type="text" class="form-control" id="contact" name="contact">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="telephone">Telefone:</label>
                                    <input type="text" class="form-control" id="telephone" name="telephone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="details">Descrição Detalhada Sobre a Associação:</label>
                                    <textarea id="summernote" name="details" cols="4">
                                        Place <em>some</em> <u>text</u> <strong>here</strong>
                                    </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="status" name="status" checked>
                            <label class="form-check-label" for="status">Activar Associação</label>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-lg">Guardar e Adicionar Nova</button>
                        <button type="submit" class="btn btn-success btn-lg">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('add-script')
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(function () {
      $('#summernote').summernote();
    })
</script>
@endsection
