<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Panel Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\Admin\MainController::class, 'index'])->name('home');

// -------------------------------- User Routes ------------------------------

Route::get('users/listusers', [App\Http\Controllers\Admin\UserController::class, 'list'])->name('users.list');
Route::get('users/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('users.edit');
Route::get('users/view/{id}', [App\Http\Controllers\Admin\UserController::class, 'view'])->name('users.view');
Route::get('users/create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('users.create');
Route::post('users/create/save', [App\Http\Controllers\Admin\UserController::class, 'save'])->name('users.save');
Route::post('users/create/update', [App\Http\Controllers\Admin\UserController::class, 'update'])->name('users.update');
//Route::post('users/create/save', [ 'as' => 'users.save', 'uses' => App\Http\Controllers\Admin\UserController::class, 'save']);

// -------------------------------- Association Routes ------------------------------

Route::get('associations/listassociations', [App\Http\Controllers\Admin\AssociationController::class, 'list'])->name('associations.list');
Route::get('associations/edit/{id}', [App\Http\Controllers\Admin\AssociationController::class, 'edit'])->name('associations.edit');
Route::get('associations/view/{id}', [App\Http\Controllers\Admin\AssociationController::class, 'view'])->name('associations.view');
Route::get('associations/create', [App\Http\Controllers\Admin\AssociationController::class, 'create'])->name('associations.create');
Route::post('associations/create/save', [App\Http\Controllers\Admin\AssociationController::class, 'save'])->name('associations.save');
Route::post('associations/create/save', [App\Http\Controllers\Admin\AssociationController::class, 'update'])->name('associations.update');

// -------------------------------- Company Routes ------------------------------

Route::get('company/listcompanies', [App\Http\Controllers\Admin\CompanyController::class, 'list'])->name('company.list');
Route::get('company/edit/{id}', [App\Http\Controllers\Admin\CompanyController::class, 'edit'])->name('company.edit');
Route::get('company/view/{id}', [App\Http\Controllers\Admin\CompanyController::class, 'view'])->name('company.view');
Route::get('company/create', [App\Http\Controllers\Admin\CompanyController::class, 'create'])->name('company.create');
Route::post('company/create/save', [App\Http\Controllers\Admin\CompanyController::class, 'save'])->name('company.save');
Route::post('company/create/save', [App\Http\Controllers\Admin\CompanyController::class, 'update'])->name('company.update');

// -------------------------------- Submission Routes ------------------------------

Route::get('submission/listsubmissions', [App\Http\Controllers\Admin\SubmissionsController::class, 'list'])->name('submission.list');
Route::get('submission/edit/{id}', [App\Http\Controllers\Admin\SubmissionsController::class, 'edit'])->name('submission.edit');
Route::get('submission/view/{id}', [App\Http\Controllers\Admin\SubmissionsController::class, 'view'])->name('submission.view');
Route::get('submission/create', [App\Http\Controllers\Admin\SubmissionsController::class, 'create'])->name('submission.create');
Route::post('submission/create/save', [App\Http\Controllers\Admin\SubmissionsController::class, 'save'])->name('submission.save');
Route::post('submission/create/save', [App\Http\Controllers\Admin\SubmissionsController::class, 'update'])->name('submission.update');



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/