<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Association extends Model{
    use HasFactory;

    protected $fillable = [
        'name',
        'initials',
        'address',
        'province_id',
        'contact',
        'fax',
        'nuit',
        'email',
        'description',
        'activity',
        'is_national',
    ];
}
