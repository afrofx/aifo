<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model{
    use HasFactory;

    protected $fillable = [
        'doc_id',
        'name',
        'birthday',
        'gender',
        'marital',
        'birthday',
        'address',
        'contact',
        'contact_2',
        'description',
        'location',
        'details',
        'province_id',
    ];
}
