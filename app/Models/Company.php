<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model{
    use HasFactory;

    protected $fillable = [
        'name',
        'initials',
        'address',
        'nuit',
        'province_id',
        'contact',
        'fax',
        'email',
        'contacto_2',
        'section',
        'description',
    ];
}
