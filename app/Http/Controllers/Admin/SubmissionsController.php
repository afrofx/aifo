<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Submission;

class SubmissionsController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function list(){
        $submissions = Submission::all();
        return view('admin.modules.submission.index', compact('submissions','provinces'));
    }

    public function create(){
        // $submissions = Submission::all();
        $posts = Array (
            "0" => Array (
                "id" => "01",
                "name" => "Hello",
            ),
            "1" => Array (
                "id" => "02",
                "name" => "Yoyo",
            ),
            "2" => Array (
                "id" => "03",
                "name" => "I like Apples",
            )
        );
        $provinces = json_encode($posts);
        return view('admin.modules.submission.create', compact('provinces'));
    }

    public function edit(Request $id){
        return view('admin.modules.submission.edit');
    }

    public function save(){
        return view('admin.modules.submission.index');
    }

    public function update(){
        return view('admin.modules.submission.index');
    }

    public function delete(){
        return view('admin.modules.submission.index');
    }
}
