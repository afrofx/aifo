<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function list(){
        $companies = Company::all();
        return view('admin.modules.company.index', compact('companies'));
    }

    public function create(){
        return view('admin.modules.company.create');
    }

    public function edit(Request $id){
        return view('admin.modules.company.edit');
    }

    public function save(){
        return view('admin.modules.company.index');
    }

    public function update(){
        return view('admin.modules.company.index');
    }

    public function delete(){
        return view('admin.modules.company.index');
    }
}
