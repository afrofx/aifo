<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Association;

class AssociationController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function list(){
        $associations = Association::all();
        return view('admin.modules.association.index', compact('associations'));
    }

    public function create(){
        return view('admin.modules.association.create');
    }

    public function edit(Request $id){
        return view('admin.modules.association.edit');
    }

    public function save(){
        return view('admin.modules.association.index');
    }

    public function update(){
        return view('admin.modules.association.index');
    }

    public function delete(){
        return view('admin.modules.association.index');
    }
}
