<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
class UserController extends Controller{
    
    public function __construct(){
        $this->middleware('auth');
    }

    public function list(){
        $users = User::all();
        return view('admin.modules.user.index', compact('users'));
    }

    public function create(){
        return view('admin.modules.user.create');
    }

    public function edit(Request $id){
        return view('admin.modules.user.edit');
    }

    public function save(Request $request){

        $request->validate([
            'firstname' => 'required',
            'surname' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'repeat-password' => 'required',
            'status' => 'required'
        ]);

        $user = new User();
        $user->name      = $request->firstname;
        $user->surname   = $request->surname;
        $user->username  = $request->username;
        $user->email     = $request->email;
        $user->password  = \Hash::make($request->password);
        if($request->status == 'on'){
            $user->status = '1';
        }else{
            $user->status = '0';
        }
        $user->save();

        return redirect()->route('users.list')->with('success', 'Usuario Adicionado Com Sucesso.');
    }

    public function update(){
        return view('admin.modules.user.index');
    }

    public function delete(){
        return view('admin.modules.user.index');
    }
}
