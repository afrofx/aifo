<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration{
    
    public function up(){
        Schema::create('submissions', function (Blueprint $table) {
            $table->id();
            $table->string('doc_id');
            $table->string('name');
            $table->string('birthday');
            $table->string('gender');
            $table->string('marital');
            $table->string('birthday');
            $table->string('address');
            $table->string('contact');
            $table->string('contact_2');
            $table->string('location');
            $table->text('description');
            $table->text('details');
            $table->int('province_id');
            $table->enum('is_active',['Sim','Nao']);
            $table->timestamps();
        });
    }
    
    public function down(){
        Schema::dropIfExists('submissions');
    }
}
