<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration{
    public function up(){
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('initials');
            $table->string('address');
            $table->int('province_id');
            $table->string('contact');
            $table->string('fax');
            $table->string('nuit');
            $table->string('email');
            $table->string('contacto_2');
            $table->text('description');
            $table->string('section');
            $table->enum('is_national',['Sim','Nao']);
            $table->enum('is_active',['Sim','Nao']);
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('companies');
    }
}
